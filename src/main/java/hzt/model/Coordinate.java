package hzt.model;

import javafx.geometry.Point2D;
import org.jetbrains.annotations.NotNull;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Coordinate extends Point2D implements Comparable<Coordinate> {

    public Coordinate(double x, double y) {
        super(x, y);
    }

    public Coordinate rotate(double angle) {
        double x = getX() * cos(angle) - getY() * sin(angle);
        double y = getX() * sin(angle) + getY() * cos(angle);
        return new Coordinate(x, y);
    }

    @Override
    public String toString() {
        return String.format("x: %4f, y: %4f\n", getX(), getY());
    }

    @Override
    public int compareTo(@NotNull Coordinate o) {
        Double thisValue = this.getX() + this.getY();
        Double otherValue = o.getX() + o.getY();
        return thisValue.compareTo(otherValue);
    }
}
