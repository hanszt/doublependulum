package hzt.model;

public enum TraceType {

    SOLID_TRACE("Solid line"),
    DOTTED_TRACE("Dotted line"),
    TRACE_ALL_POS1("All positions type 1"),
    TRACE_ALL_POS2("All positions type 2"),
    NO_TRACE("No Trace");

    private final String text;

    TraceType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
