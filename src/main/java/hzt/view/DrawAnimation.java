package hzt.view;

import hzt.controller.AppManager;
import hzt.model.TraceType;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hzt.model.TraceType.DOTTED_TRACE;
import static hzt.model.TraceType.TRACE_ALL_POS2;

public class DrawAnimation extends UpdateVars {

    private static final Logger LOGGER = LoggerFactory.getLogger(DrawAnimation.class);
    private TraceType curTraceType = DOTTED_TRACE;

    private SliderUpdater posSliderUpdater;

    public DrawAnimation(AppManager appManager, ColorController c) {
        super(appManager, c);
    }

    public void drawPendulum(Canvas canvas, Canvas traceCanvas) {
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        if (curTraceType != TRACE_ALL_POS2) {
            graphics.clearRect(0, 0, m.getScene().getWidth(), m.getScene().getHeight());
        }
        update();
        translateGraphics(1, canvas, traceCanvas);
        drawCurrentPos(canvas, traceCanvas);
        translateGraphics(-1, canvas, traceCanvas);
        if (m.getUserInputController().getFollowTipButton().isActive()) {
            final var x = mass2Pos.getX() * m.getCanvas().getScaleX();
            final var y = mass2Pos.getY() * m.getCanvas().getScaleY();
            getPosSliderUpdater().updateSliders(x, y, 0, 0);
        }
    }

    private void translateGraphics(int dir, Canvas... canvasArray) {
        for (Canvas c : canvasArray) {
            GraphicsContext graphics = c.getGraphicsContext2D();
            graphics.translate(origin.getX() * dir, origin.getY() * dir);
        }
    }

    private void drawCurrentPos(Canvas canvas, Canvas traceCanvas) {
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        drawSelectedTrace(traceType, graphics);
        drawTotalTrace(traceCanvas.getGraphicsContext2D());
        graphics.setStroke(c.pendulumColor);
        graphics.setFill(c.ballColor);
        graphics.setLineWidth(pendulumThickness);
        if (traceType == TRACE_ALL_POS2) {
            drawTraceAll2(graphics);
        } else {
            strokePendulumPos(mass1Pos, mass2Pos, graphics);
        }
        curTraceType = traceType;
        double scaledDiamAttachment = DIAM_ATTACHMENT;
        double scaledRAttachment = scaledDiamAttachment / 2.;
        graphics.fillOval(-scaledRAttachment, -scaledRAttachment, scaledDiamAttachment, scaledDiamAttachment);
    }

    private void drawSelectedTrace(TraceType traceType, GraphicsContext graphics) {
        switch (traceType) {
            case DOTTED_TRACE -> drawTrace(graphics, true);
            case SOLID_TRACE -> drawTrace(graphics, false);
            case TRACE_ALL_POS1 -> drawTraceAll1(graphics);
            default -> LOGGER.trace("traceType: {}", traceType);
        }
    }

    private void drawTotalTrace(GraphicsContext graphics) {
        graphics.setStroke(c.ballColor);
        graphics.strokeLine(prevMass1.getX(), prevMass1.getY(), mass1Pos.getX(), mass1Pos.getY());
        graphics.setStroke(c.traceColor);
        graphics.strokeLine(prevMass2.getX(), prevMass2.getY(), mass2Pos.getX(), mass2Pos.getY());
    }

    private void drawTrace(GraphicsContext graphics, boolean dots) {
        int size = traceList.size();
        for (int i = 0; i < size - 1; i++) {
            Point2D currentM1 = traceList.get(i).getKey();
            Point2D nextM1 = traceList.get(i + 1).getKey();
            Point2D currentM2 = traceList.get(i).getValue();
            Point2D nextM2 = traceList.get(i + 1).getValue();
            if (dots) {
                graphics.setFill(c.ballColor);
                graphics.fillOval(currentM1.getX(), currentM1.getY(), 1, 1);
                graphics.setFill(c.traceColor);
                graphics.fillOval(currentM2.getX() - 1.5, currentM2.getY() - 1.5, 3, 3);
            } else {
                graphics.setLineWidth(1);
                graphics.setStroke(c.ballColor);
                graphics.strokeLine(currentM1.getX(), currentM1.getY(), nextM1.getX(), nextM1.getY());
                graphics.setStroke(c.traceColor);
                graphics.strokeLine(currentM2.getX(), currentM2.getY(), nextM2.getX(), nextM2.getY());
            }
        }
    }

    private void drawTraceAll1(GraphicsContext graphics) {
        int size = traceList.size();
        for (int i = 1; i < size; i++) {
            final var connection = traceList.get(i);
            Point2D currentM1 = connection.getKey();
            Point2D currentM2 = connection.getValue();
            graphics.setLineWidth(pendulumThickness);
            graphics.setStroke(c.pendulumColor.darker().darker());
            graphics.setFill(c.ballColor.darker().darker());
            strokePendulumPos(currentM1, currentM2, graphics);
        }
    }

    private void drawTraceAll2(GraphicsContext graphics) {
        graphics.setLineWidth(pendulumThickness);
        graphics.setStroke(c.pendulumColor.darker().darker());
        graphics.setFill(c.ballColor.darker().darker());
        strokePendulumPos(prevMass1, prevMass2, graphics);
        graphics.setStroke(c.pendulumColor);
        graphics.setFill(c.ballColor);
        strokePendulumPos(mass1Pos, mass2Pos, graphics);
    }

    private void strokePendulumPos(Point2D mass1, Point2D mass2, GraphicsContext graphics) {
        double scaledDiamMass1 = diamMass1;
        double scaledDiamMass2 = diamMass2;
        double scaledRadiusMass1 = scaledDiamMass1 / 2;
        double scaledRadiusMass2 = scaledDiamMass2 / 2;
        graphics.strokeLine(0, 0, mass1.getX(), mass1.getY());
        graphics.strokeLine(mass1.getX(), mass1.getY(), mass2.getX(), mass2.getY());
        graphics.fillOval(mass1.getX() - scaledRadiusMass1, mass1.getY() - scaledRadiusMass1, scaledDiamMass1, scaledDiamMass1);
        graphics.fillOval(mass2.getX() - scaledRadiusMass2, mass2.getY() - scaledRadiusMass2, scaledDiamMass2, scaledDiamMass2);
    }

    public SliderUpdater getPosSliderUpdater() {
        return posSliderUpdater;
    }

    public void setPosSliderUpdater(SliderUpdater posSliderUpdater) {
        this.posSliderUpdater = posSliderUpdater;
    }
}
