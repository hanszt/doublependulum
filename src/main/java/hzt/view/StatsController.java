package hzt.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class StatsController {

    private final DrawAnimation d;
    private final ColorController c;

    public StatsController(DrawAnimation d, ColorController c) {
        this.d = d;
        this.c = c;
    }

    private static final int FONT_SIZE_STATS = 10;
    private static final int NUMBER_POS = 140;

    private void drawVarNames(GraphicsContext graphics) {
        graphics.setTextAlign(TextAlignment.LEFT);
        graphics.setFont(Font.font("", FONT_SIZE_STATS));
        graphics.setFill(c.ballColor);
        graphics.fillText("Double pendulum stats:\n" +
                "Pendulum1 length: \n" +
                "Pendulum2 length: \n" +
                "Mass1 (m1): \n" +
                "Mass2 (m2): \n" +
                "Position m1 (x, y): \n" +
                "Position m2 (x, y): \n" +
                "Angle1: \n" +
                "Angle2: \n" +
                "Angular vel m1: \n" +
                "Angular vel m2: \n" +
                "Angular acc m1: \n" +
                "Angular acc m2: \n" +
                "Friction: \n" +
                "Runtime: ", 5, FONT_SIZE_STATS);
    }

    public void getStats(double runTimeSim, Canvas canvas) {
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        if (d.m.getUserInputController().showStatsButton.isActive()) {
            drawVarNames(graphics);
            graphics.setFill(c.traceColor);
            double angle1 = d.angle1 * AnimationVars.FROM_RAD_TO_DEG;
            double angle2 = d.angle2 * AnimationVars.FROM_RAD_TO_DEG;
            double vAng1 = d.vAngular1 * AnimationVars.FROM_RAD_TO_DEG;
            double vAng2 = d.vAngular2 * AnimationVars.FROM_RAD_TO_DEG;
            double aAng1 = d.aAngular1 * AnimationVars.FROM_RAD_TO_DEG;
            double aAng2 = d.aAngular2 * AnimationVars.FROM_RAD_TO_DEG;
            graphics.fillText(String.format("\n%.2f\n%.2f\n%d\n%d\n(%3.0f, %3.0f)\n(%3.0f, %3.0f)" +
                            "\n%3.0f deg\n%3.0f deg\n%2.2f deg/s\n%2.2f deg/s\n%2.2f deg/s^2\n%2.2f deg/s^2\n%1.3f\n%3.1f s",
                    d.pendulum1Length, d.pendulum2Length, d.m1, d.m2,
                    d.mass1Pos.getX(), d.mass1Pos.getY(), d.mass2Pos.getX(), d.mass2Pos.getY(),
                    angle1, angle2, vAng1, vAng2, aAng1, aAng2, d.friction, runTimeSim),
                    NUMBER_POS, FONT_SIZE_STATS);
        }
    }
}
