package hzt.view;

import hzt.controller.AppManager;
import hzt.model.Coordinate;
import javafx.geometry.Point2D;
import javafx.util.Pair;

import static java.lang.Math.*;

abstract class UpdateVars extends AnimationVars {

    SliderUpdater pendulumSliderUpdater;

    UpdateVars(AppManager m, ColorController c) {
     super(m, c);
    }

    void update() {
        prevMass1 = mass1Pos;
        prevMass2 = mass2Pos;
        pendulumSliderUpdater.updateSliders(angle1, angle2, vAngular1, vAngular2);
        calculateAngularAccM1();
        calculateAngularAccM2();
        calculateAngleVars();
        setBoundToAngles();
        updateCoordinates();
        updateTraceList();
        simulateFriction();
        regulateMassSizes();
    }

    private void updateCoordinates() {
        mass1Pos = new Coordinate(sin(angle1) * pendulum1Length, cos(angle1) * pendulum1Length);
        Point2D temp = new Coordinate(sin(angle2) * pendulum2Length, cos(angle2) * pendulum2Length);
        mass2Pos = new Coordinate(mass1Pos.getX() + temp.getX(), mass1Pos.getY() + temp.getY());
    }

    private void updateTraceList() {
        traceList.add(new Pair<>(mass1Pos, mass2Pos));
        if (traceList.size() >= MAX_TRACE_LIST_SIZE) {
            traceList.remove(0);
        }
    }

    private void calculateAngularAccM1() {
        double num1 = -G * (2 * m1 + m2) * sin(angle1);
        double num2 = -m2 * G * sin(angle1 - 2 * angle2);
        double num3 = -2 * sin(angle1 - angle2) * m2;
        double num4 = vAngular2 * vAngular2 * pendulum2Length + vAngular1 * vAngular1 * pendulum1Length * cos(angle1 - angle2);
        double den = pendulum1Length * (2 * m1 * m2 - m2 * cos(2 * angle1 - 2 * angle2));
        aAngular1 = (num1 + num2 + num3 * num4) / den;

    }

    private void calculateAngularAccM2() {
        double num1 = 2 * sin(angle1 - angle2);
        double num2 = vAngular1 * vAngular1 * pendulum1Length * (m1 + m2);
        double num3 = G * (m1 + m2) * cos(angle1);
        double num4 = vAngular2 * vAngular2 * pendulum2Length * m2 * cos(angle1 - angle2);
        double den = pendulum2Length * (2 * m1 * m2 - m2 * cos(2 * angle1 - 2 * angle2));
        aAngular2 = (num1 * (num2 + num3 + num4)) / den;
    }

    private void calculateAngleVars() {
        angle1 += vAngular1 += aAngular1;
        angle2 += vAngular2 += aAngular2;
    }

    private void setBoundToAngles() {
        if (angle1 > PI) {
            angle1 = angle1 - 2 * PI;
        }
        if (angle1 < -PI) {
            angle1 = angle1 + 2 * PI;
        }
        if (angle2 > PI){
            angle2 = angle2 - 2 * PI;
        }
        if (angle2 < -PI) {
            angle2 = angle2 + 2 * PI;
        }
    }

    private void simulateFriction() {
        vAngular1 *= 1 - friction;
        vAngular2 *= 1 - friction;
    }

    private void regulateMassSizes() {
        diamMass1 = BALL_DIAM_FACTOR * pow(m1, 1./3);
        diamMass2 = BALL_DIAM_FACTOR * pow(m2, 1./3);
    }
}
