package hzt.view;

public interface SliderUpdater {

    void updateSliders(double var1, double var2, double var3, double var4);
}
