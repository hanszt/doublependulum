package hzt.view;

import javafx.scene.paint.Color;

import java.util.Random;

import static javafx.scene.paint.Color.*;

public class ColorController {

    private final Random random = new Random();

    Color backGroundColor = DARKGREEN.darker().darker().darker();
    Color pendulumColor = getRandomColor();
    Color ballColor = WHEAT;
    Color traceColor = getRandomColor();

    private Color getRandomColor() {
        Color color;
        int selector = random.nextInt(8);
        if (selector == 0) color = MAROON;
        else if (selector == 1) color = ORANGE;
        else if (selector == 2) color = YELLOW;
        else if (selector == 3) color = LIGHTGREEN;
        else if (selector == 4) color = SADDLEBROWN;
        else if (selector == 6) color = GREEN;
        else if (selector == 7) color = RED;
        else color = TURQUOISE;
        return color;
    }

    public void resetColors() {
        pendulumColor = getRandomColor();
        ballColor = WHEAT;
    }

    public Color getBackGroundColor() {
        return backGroundColor;
    }

    public Color getPendulumColor() {
        return pendulumColor;
    }
}
