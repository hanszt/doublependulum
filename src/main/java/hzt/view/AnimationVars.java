package hzt.view;

import hzt.controller.AppConstants;
import hzt.controller.AppManager;
import hzt.model.Coordinate;
import hzt.model.TraceType;
import javafx.geometry.Point2D;
import javafx.util.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static hzt.model.TraceType.DOTTED_TRACE;
import static java.lang.Math.*;

abstract class AnimationVars {

    //sim constants
    private static final int INIT_PENDULUM_THICKNESS = 12;
    static final int MIN_PENDULUM_LENGTH = 1;
    static final int MAX_PENDULUM_LENGTH = 200;
    static final int MIN_MASS = 5;
    static final int MAX_MASS = 1000;
    static final int MIN_START_ANGLE = -180;
    static final int MAX_START_ANGLE = 180;
    static final int MIN_V_ANGULAR = -20;
    static final int MAX_V_ANGULAR = 20;
    static final int MAX_TRACE_LIST_SIZE = 1200;
    static final int BALL_DIAM_FACTOR = 10;
    private static final double INIT_FRICTION = 0.002;

    static final double G = 9.81;
    static final double FROM_RAD_TO_DEG = 180 / PI;
    private static final double FROM_DEG_TO_RAD = PI / 180;
    static final double DIAM_ATTACHMENT = 20;

    final AppManager m;
    final ColorController c;
    private final int initialPendulum1Length;
    private final int initialPendulum2Length;
    private final int initialM1;
    private final int initialM2;
    private final double initialAngle1;
    private final double initialAngle2;

    AnimationVars(AppManager m, ColorController c) {
        this.m = m;
        this.c = c;

        initialPendulum1Length = getRandomInteger(MIN_PENDULUM_LENGTH, MAX_PENDULUM_LENGTH);
        initialPendulum2Length = getRandomInteger(MIN_PENDULUM_LENGTH, MAX_PENDULUM_LENGTH);
        initialM1 = getRandomInteger(MIN_MASS, MAX_MASS);
        initialM2 = getRandomInteger(MIN_MASS, MAX_MASS);
        initialAngle1 = getRandomInteger(MIN_START_ANGLE, MAX_START_ANGLE) * FROM_DEG_TO_RAD;
        initialAngle2 = getRandomInteger(MIN_START_ANGLE, MAX_START_ANGLE) * FROM_DEG_TO_RAD;
        setAnimationVars();
        traceList = new LinkedList<>();
    }

    Point2D origin = AppConstants.INIT_ORIGIN;
    TraceType traceType = DOTTED_TRACE;

    double pendulum1Length;
    double pendulum2Length;
    int m1;
    int m2;

    double pendulumThickness = INIT_PENDULUM_THICKNESS;
    double diamMass1;
    double diamMass2;

    Point2D prevMass1;
    Point2D prevMass2;
    Point2D mass1Pos;
    Point2D mass2Pos;

    double angle1;
    double angle2;
    double vAngular1;
    double vAngular2;
    double aAngular1;
    double aAngular2;
    double friction;

    final List<Pair<Point2D, Point2D>> traceList;

    private void setAnimationVars() {
        m1 = initialM1;
        m2 = initialM2;
        pendulum1Length = initialPendulum1Length;
        pendulum2Length = initialPendulum2Length;
        friction = INIT_FRICTION;
        angle1 = initialAngle1;
        angle2 = initialAngle2;
        prevMass1 = mass1Pos = new Coordinate(cos(angle1) * pendulum1Length, sin(angle1) * pendulum1Length);
        prevMass2 = mass2Pos = new Coordinate(mass1Pos.getX() + cos(angle2) * pendulum2Length, mass1Pos.getY() + sin(angle2) * pendulum2Length);
    }

    private final Random random = new Random();

    private int getRandomInteger(int min, int max) {
        return random.nextInt(max - min) + min;
    }

    SliderUpdater initialSliderPosUpdater;
    ComboBoxUpdater<TraceType> traceMenuUpdater;

    public void resetAnimationVars(boolean isRestart) {
        if(isRestart) {
            initialSliderPosUpdater.updateSliders(0, 0, 0, 0);
            setAnimationVars();
        }
        traceType = DOTTED_TRACE;
        traceMenuUpdater.update(traceType);
        vAngular1 = vAngular2 = aAngular1 = aAngular2 = 0;
        m.getUserInputController().getFollowTipButton().setActive(false);
        traceList.clear();
    }

    public Point2D getOrigin() {
        return origin;
    }

    public void setOrigin(Point2D origin) {
        this.origin = origin;
    }
}
