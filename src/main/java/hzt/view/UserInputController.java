package hzt.view;

import hzt.controller.AppManager;
import hzt.model.TraceType;
import hzt.model.custom_controls.SwitchButton;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.EnumSet;

import static hzt.controller.AppConstants.PREF_WIDTH_MENU;
import static hzt.controller.AppConstants.SCREEN_SIZE;
import static hzt.view.AnimationVars.*;

public class UserInputController {

    private static final int DIST_FROM_EDGE = 20;

    private final AppManager appManager;
    private final DrawAnimation drawAnimation;
    private final ColorController colorController;

    public UserInputController(AppManager appManager, DrawAnimation drawAnimation,
                               ColorController colorController) {
        this.appManager = appManager;
        this.drawAnimation = drawAnimation;
        this.colorController = colorController;
    }

    private int vPosControls;
    private static final double OPACITY_VALUE = 0.7;
    private double controlsOpacity = OPACITY_VALUE;

    public GridPane setupControls() {
        GridPane mainGrid = setupGridPane();
        setupLabels(mainGrid);
        setupTraceDropdownMenu(mainGrid);
        setupButtons(mainGrid);
        setupColorPickers(mainGrid);
        setupPendulumSliders(mainGrid);
        setupPosSliders(mainGrid);
        for (Node n : mainGrid.getChildren()) setGlobalProps(n);
        vPosControls = 0;
        return mainGrid;
    }

    private void setupLabels(GridPane grid) {
        Label label1 = new Label();
        Label label2 = new Label();
        label1.setPrefWidth(PREF_WIDTH_MENU);
        label1.setPrefHeight(SCREEN_SIZE.height / 2.);
        label2.setPrefWidth(SCREEN_SIZE.width * 2 / 3.);
        grid.add(label1, 0, vPosControls++, 2, 1);
        grid.add(label2, 2, 0, 2, 1);
    }

    private void setGlobalProps(Node n) {
        n.setOpacity(controlsOpacity);
        n.setCursor(Cursor.HAND);
    }

    private GridPane setupGridPane() {
        GridPane grid = new GridPane();
        grid.setHgap(2);
        grid.setVgap(4);
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setPadding(new Insets(DIST_FROM_EDGE, DIST_FROM_EDGE, DIST_FROM_EDGE * 2., DIST_FROM_EDGE));
        return grid;
    }

    private void setupTraceDropdownMenu(GridPane grid) {
        final ComboBox<TraceType> sortByOptions = new ComboBox<>();
        sortByOptions.setPrefWidth(PREF_WIDTH_MENU);
        sortByOptions.setConverter(getStringConverter());
        for (TraceType c : EnumSet.allOf(TraceType.class)) sortByOptions.getItems().add(c);
        sortByOptions.setValue(drawAnimation.traceType);
        sortByOptions.setOnAction(e -> drawAnimation.traceType = sortByOptions.getValue());
        grid.add(sortByOptions, 0, vPosControls++, 2, 1);
        drawAnimation.traceMenuUpdater = sortByOptions::setValue;
    }

    private <T> StringConverter<T> getStringConverter() {
        return new StringConverter<T>() {
            @Override
            public String toString(T item) {
                if (item instanceof TraceType) return ((TraceType) item).getText();
                else return item.toString();
            }

            @Override
            public T fromString(String id) {
                return null;
            }
        };
    }

    SwitchButton showStatsButton;
    SwitchButton opacityButton;
    SwitchButton followTipButton;
    SwitchButton showTotalTraceButton;

    private void setupButtons(GridPane grid) {
        Button restart = setupButton(grid, "restart sim", 0, vPosControls);
        Button reset = setupButton(grid, "reset sim", 1, vPosControls++);
        SwitchButton pauseButton = setupSwitchButton(grid, false, "play", "pause", 0, vPosControls);
        Button clearTraceButton = setupButton(grid, "clear trace", 1, vPosControls++);
        showStatsButton = setupSwitchButton(grid, false, "hide stats", "show stats", 0, vPosControls);
        opacityButton = setupSwitchButton(grid, true, "set solid", "set transparent", 1, vPosControls++);
        followTipButton = setupSwitchButton(grid, false, "fixed camera", "follow draw spot", 0, vPosControls);
        Button reCenter = setupButton(grid, "recenter", 1, vPosControls++);
        Button newInstance = setupButton(grid, "start new instance", 0, vPosControls);
        showTotalTraceButton = setupSwitchButton(grid, false, "hide total Trace", "show total trace", 1, vPosControls++);
        restart.setOnAction(e -> appManager.restartSim());
        reset.setOnAction(e -> appManager.reset(false));
        pauseButton.getMultipleEventsHandler().addEventHandler(0, e -> {
            if (pauseButton.isActive()) appManager.getTimeline().play();
            else appManager.getTimeline().pause();
        });
        clearTraceButton.setOnAction(e -> {
            drawAnimation.traceList.clear();
            appManager.getTraceCanvas().getGraphicsContext2D().clearRect(0, 0, appManager.getScene().getWidth(), appManager.getScene().getHeight());
        });
        opacityButton.getMultipleEventsHandler().addEventHandler(0, e -> {
            controlsOpacity = controlsOpacity == OPACITY_VALUE ? 1 : OPACITY_VALUE;
            for (Node n : grid.getChildren()) n.setOpacity(controlsOpacity);
            appManager.updateFrameIfPaused();
        });
        showTotalTraceButton.getMultipleEventsHandler().addEventHandler(0, e -> appManager.getTraceCanvas().setOpacity(showTotalTraceButton.isActive() ? 0 : 1));
        reCenter.setOnAction(e -> {
            followTipButton.setActive(false);
            drawAnimation.getPosSliderUpdater().updateSliders(0, 0, 0, 0);
        });
        newInstance.setOnAction(e -> new AppManager(new Stage()));
    }

    private SwitchButton setupSwitchButton(GridPane grid, boolean on, String onText, String offText, int hPos, int vPos) {
        SwitchButton switchButton = new SwitchButton(on, onText, offText);
        switchButton.setPrefWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setMinWidth(PREF_WIDTH_MENU / 2.);
        switchButton.getMultipleEventsHandler().addEventHandler(e -> appManager.updateFrameIfPaused());
        grid.add(switchButton, hPos, vPos);
        return switchButton;
    }

    private Button setupButton(GridPane grid, String text, int hPos, int vPos) {
        Button button = new Button(text);
        button.setPrefWidth(PREF_WIDTH_MENU / 2.);
        grid.add(button, hPos, vPos, 1, 1);
       return button;
    }

    private void setupColorPickers(GridPane grid) {
        ColorPicker pendulumColorPicker = setupColorPicker(colorController.pendulumColor, grid, 0, vPosControls);
        ColorPicker ballColorPicker = setupColorPicker(colorController.ballColor, grid, 1, vPosControls++);
        ColorPicker backgroundColorPicker = setupColorPicker(colorController.backGroundColor, grid, 0, vPosControls);
        ColorPicker traceColorPicker = setupColorPicker(colorController.traceColor, grid, 1, vPosControls++);
        pendulumColorPicker.setOnAction(e -> colorController.pendulumColor = pendulumColorPicker.getValue());
        ballColorPicker.setOnAction(e -> colorController.ballColor = ballColorPicker.getValue());
        backgroundColorPicker.setOnAction(e -> {
            Color bgColor = backgroundColorPicker.getValue();
            colorController.backGroundColor = bgColor;
            appManager.getRoot().setBackground(new Background(new BackgroundFill(bgColor, CornerRadii.EMPTY, Insets.EMPTY)));
        });
        traceColorPicker.setOnAction(e -> colorController.traceColor = traceColorPicker.getValue());
    }

    private ColorPicker setupColorPicker(Color startColor, GridPane grid, int hPos, int vPos) {
        ColorPicker colorPicker = new ColorPicker(startColor);
        colorPicker.setPrefWidth(PREF_WIDTH_MENU / 2.);
        colorPicker.setStyle("-fx-color-label-visible: false ;");
        grid.add(colorPicker, hPos, vPos, 1, 1);
        return colorPicker;
    }

    private void setupPendulumSliders(GridPane grid) {
        Slider sliderAngle1 = setupSlider(grid, MIN_START_ANGLE, MAX_START_ANGLE, drawAnimation.angle1 * FROM_RAD_TO_DEG, vPosControls++, true);
        Slider sliderAngle2 = setupSlider(grid, MIN_START_ANGLE, MAX_START_ANGLE, drawAnimation.angle2 * FROM_RAD_TO_DEG, vPosControls++, true);
        Slider sliderVAngular1 = setupSlider(grid, MIN_V_ANGULAR, MAX_V_ANGULAR, drawAnimation.vAngular1 * FROM_RAD_TO_DEG, vPosControls++, true);
        Slider sliderVAngular2 = setupSlider(grid, MIN_V_ANGULAR, MAX_V_ANGULAR, drawAnimation.vAngular2 * FROM_RAD_TO_DEG, vPosControls++, true);
        Slider sliderPen1 = setupSlider(grid, MIN_PENDULUM_LENGTH, MAX_PENDULUM_LENGTH, drawAnimation.pendulum1Length, vPosControls++, true);
        Slider sliderPen2 = setupSlider(grid, MIN_PENDULUM_LENGTH, MAX_PENDULUM_LENGTH, drawAnimation.pendulum2Length, vPosControls++, true);
        Slider sliderM1 = setupSlider(grid, MIN_MASS, MAX_MASS, drawAnimation.m1, vPosControls++, true);
        Slider sliderM2 = setupSlider(grid, MIN_MASS, MAX_MASS, drawAnimation.m2, vPosControls++, true);
        Slider sliderFriction = setupSlider(grid, 0, 0.2, drawAnimation.friction, vPosControls, false);
        sliderAngle1.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.angle1 = newVal.doubleValue() / FROM_RAD_TO_DEG);
        sliderAngle2.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.angle2 = newVal.doubleValue() / FROM_RAD_TO_DEG);
        sliderVAngular1.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.vAngular1 = newVal.doubleValue() / FROM_RAD_TO_DEG);
        sliderVAngular2.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.vAngular2 = newVal.doubleValue() / FROM_RAD_TO_DEG);
        sliderPen1.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.pendulum1Length = newVal.doubleValue());
        sliderPen2.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.pendulum2Length = newVal.doubleValue());
        sliderM1.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.m1 = newVal.intValue());
        sliderM2.valueProperty().addListener((prevVal, value, newVal) -> drawAnimation.m2 = newVal.intValue());
        sliderFriction.valueProperty().addListener((o, c, n) -> drawAnimation.friction = n.doubleValue());
        drawAnimation.pendulumSliderUpdater = (angle1, angle2, vAng1, vAng2) -> {
            sliderAngle1.setValue(angle1 * FROM_RAD_TO_DEG);
            sliderAngle2.setValue(angle2 * FROM_RAD_TO_DEG);
            sliderVAngular1.setValue(vAng1 * FROM_RAD_TO_DEG);
            sliderVAngular2.setValue(vAng2 * FROM_RAD_TO_DEG);
        };
        drawAnimation.initialSliderPosUpdater = (v1, v2, v3, v4) -> {
            sliderAngle1.setValue(drawAnimation.angle1);
            sliderAngle2.setValue(drawAnimation.angle2);
            sliderPen1.setValue(drawAnimation.pendulum1Length);
            sliderPen2.setValue(drawAnimation.pendulum2Length);
            sliderM1.setValue(drawAnimation.m1);
            sliderM2.setValue(drawAnimation.m2);
            sliderFriction.setValue(drawAnimation.friction);
        };
    }

    private static final double MIN_SCALE_FACTOR = 0.05;
    private static final double MAX_SCALE_FACTOR = 10;

    private void setupPosSliders(GridPane grid) {
        double width = appManager.getScene().getWidth();
        double height = appManager.getScene().getHeight();
        Slider zoomSlider = setupPositionSlider(grid, new SliderInput(MIN_SCALE_FACTOR, MAX_SCALE_FACTOR, 1, 2, vPosControls, 2, 1), false);
        Slider horizontalOrr = setupPositionSlider(grid, new SliderInput(-width, width, 0, 4, vPosControls, 2, 1), false);
        Slider verticalOrr = setupPositionSlider(grid, new SliderInput(-height, height, 0, 6, 0, 1, vPosControls), true);
        appManager.getCanvas().scaleXProperty().bind(zoomSlider.valueProperty());
        appManager.getCanvas().scaleYProperty().bind(zoomSlider.valueProperty());
        appManager.getCanvas().translateXProperty().bind(horizontalOrr.valueProperty().multiply(-1));
        appManager.getCanvas().translateYProperty().bind(verticalOrr.valueProperty());
        drawAnimation.setPosSliderUpdater((x, y, v1, v2) -> {
            horizontalOrr.setValue(x);
            verticalOrr.setValue(y);
        });
        zoomSliderUpdater = (zoom, var1, var2, var3) -> zoomSlider.setValue(zoom);
    }

    private Slider setupSlider(GridPane grid, double min, double max, double cur, int vPos, boolean hasScale) {
        Slider slider = new Slider(min, max, cur);
        slider.setPrefWidth(PREF_WIDTH_MENU);
        if (hasScale) {
            slider.setShowTickLabels(true);
            slider.setShowTickMarks(true);
            slider.setMajorTickUnit(50);
            slider.setMinorTickCount(5);
            slider.setBlockIncrement(10);
        }
        grid.add(slider, 0, vPos, 2, 1);
        return slider;
    }

    private Slider setupPositionSlider(GridPane grid, SliderInput s, boolean isVertical) {
        Slider slider = new Slider(s.minValue, s.maxValue, s.curValue);
        if (isVertical) {
            slider.setPrefHeight(appManager.getScene().getHeight() - (DIST_FROM_EDGE * 2));
            slider.setOrientation(Orientation.VERTICAL);
        } else slider.setPrefWidth(appManager.getScene().getWidth() - DIST_FROM_EDGE * 2);
        grid.add(slider, s.hPos, s.vPos, s.hSpan, s.vSpan);
        return slider;
    }

    private static class SliderInput {

        private final int hPos;
        private final int vPos;
        private final int hSpan;
        private final int vSpan;
        private final double minValue;
        private final double maxValue;
        private final double curValue;

        private SliderInput(double minValue, double maxValue, double curValue, int hPos, int vPos, int hSpan, int vSpan) {
            this.hPos = hPos;
            this.vPos = vPos;
            this.hSpan = hSpan;
            this.vSpan = vSpan;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.curValue = curValue;
        }
    }

    private SliderUpdater zoomSliderUpdater;

    public void addMouseScrolling(Scene scene) {
        scene.setOnScroll(e -> {
            // Adjust the zoom factor as per your requirement
            double zoomFactor = 1;
            if (appManager.getCanvas().getScaleX() > MIN_SCALE_FACTOR && e.getDeltaY() < 0) zoomFactor = 0.9;
            else if ((appManager.getCanvas().getScaleX() < MAX_SCALE_FACTOR && e.getDeltaY() > 0)) zoomFactor = 1.1;
            zoomSliderUpdater.updateSliders(appManager.getCanvas().getScaleX() * zoomFactor, 0, 0, 0);
        });
    }


    public SwitchButton getFollowTipButton() {
        return followTipButton;
    }
}
