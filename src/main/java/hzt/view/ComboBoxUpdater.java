package hzt.view;

public interface ComboBoxUpdater<T> {

    void update(T var);
}
