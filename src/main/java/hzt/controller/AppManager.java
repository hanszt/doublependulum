package hzt.controller;

import hzt.model.Coordinate;
import hzt.view.ColorController;
import hzt.view.DrawAnimation;
import hzt.view.StatsController;
import hzt.view.UserInputController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static java.lang.System.nanoTime;
import static javafx.animation.Animation.INDEFINITE;
import static javafx.animation.Animation.Status.PAUSED;
import static javafx.util.Duration.seconds;

public class AppManager extends AppConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppManager.class);
    private static int instances = 0;
    private final int instance;

    private final ColorController colorController = new ColorController();
    private final DrawAnimation drawAnimation = new DrawAnimation(this, colorController);
    private final StatsController statsController = new StatsController(drawAnimation, colorController);
    private final UserInputController userInputController = new UserInputController(this, drawAnimation, colorController);

    private final Scene scene;
    private final StackPane root;
    private final Timeline timeline;
    private final Canvas canvas;
    private final Canvas traceCanvas;

    private Point2D initP;

    private double startTimeSim;
    private double runTimeSim;
    private double tStart;
    private final ChangeListener<? super Number> widthListener = (ChangeListener<Number>) (o, c, n) ->
            drawAnimation.setOrigin(new Point2D(n.doubleValue() / 2, drawAnimation.getOrigin().getY()));

    private final ChangeListener<? super Number> heightListener = (ChangeListener<Number>) (o, c, n) ->
            drawAnimation.setOrigin(new Point2D(drawAnimation.getOrigin().getX(), n.doubleValue() / 2));
    public AppManager(Stage stage) {
        instance = ++instances;
        root = new StackPane();
        scene = new Scene(root, INIT_SCENE_SIZE.width, INIT_SCENE_SIZE.height);
        canvas = new Canvas();
        traceCanvas = new Canvas();
        timeline = new Timeline(new KeyFrame(seconds(1. / INIT_FRAME_RATE), event -> manageSim(canvas, traceCanvas)));
        setAppProperties(stage);
        setupStage(stage);
        start();
    }

    private static void bindCanvasToBaseCanvas(Canvas canvas, Canvas baseCanvas) {
        canvas.widthProperty().bindBidirectional(baseCanvas.widthProperty());
        canvas.heightProperty().bindBidirectional(baseCanvas.heightProperty());
        canvas.translateXProperty().bindBidirectional(baseCanvas.translateXProperty());
        canvas.translateYProperty().bindBidirectional(baseCanvas.translateYProperty());
        canvas.scaleXProperty().bindBidirectional(baseCanvas.scaleXProperty());
        canvas.scaleYProperty().bindBidirectional(baseCanvas.scaleYProperty());
    }

    private void setAppProperties(Stage stage) {
        root.setAlignment(Pos.CENTER);
        timeline.setCycleCount(INDEFINITE);
        final GridPane controlsGrid = userInputController.setupControls();
        canvas.widthProperty().bind(stage.widthProperty());
        canvas.heightProperty().bind(stage.heightProperty());
        bindCanvasToBaseCanvas(traceCanvas, canvas);
        traceCanvas.setOpacity(0);
        root.getChildren().addAll(canvas, traceCanvas,  controlsGrid);
        root.setBackground(new Background(new BackgroundFill(colorController.getBackGroundColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        addResponsiveSceneMovement(scene);
    }

    private void setupStage(Stage stage) {
        stage.setScene(scene);
        stage.setTitle(String.format("%s (%d)%n", TITLE, instance));
        stage.setMinWidth(500);
        stage.setMinHeight(500);
        stage.show();
        stage.widthProperty().addListener(widthListener);
        stage.heightProperty().addListener(heightListener);
        stage.setOnCloseRequest(e -> printClosingText());
    }

    private void start() {
        LOGGER.info(TITLE_FX);
        setupStartScreen(canvas);
        tStart = nanoTime();
        timeline.play();
        final var lineSeparator = System.lineSeparator();
        LOGGER.info("Animation of instance {} starting...{}", instance, lineSeparator);

    }

    private void addResponsiveSceneMovement(Scene scene) {
        scene.setOnMousePressed(e ->
                initP = new Coordinate(-canvas.getTranslateX() + e.getX(), -canvas.getTranslateY() + e.getY()));
        scene.setOnMouseDragged(this::onMouseDragged);
        userInputController.addMouseScrolling(scene);
    }

    private void onMouseDragged(MouseEvent e) {
        userInputController.getFollowTipButton().setActive(false);
        double deltaX = e.getX() - initP.getX();
        double deltaY = e.getY() - initP.getY();
        //first update the sliders, and then the actual parameters in sim. Then there will not be an interaction!
        drawAnimation.getPosSliderUpdater().updateSliders(-deltaX, deltaY, 0, 0);
    }

    private void setupStartScreen(Canvas canvas) {
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        graphics.setLineWidth(1);
        graphics.setStroke(colorController.getPendulumColor());
        graphics.setTextAlign(TextAlignment.CENTER);
        graphics.setFont(new Font("", FONT_SIZE_ANNOUNCEMENTS));
        graphics.strokeText(TITLE, scene.getWidth() / 2, scene.getHeight() / 2);
    }

    private void manageSim(Canvas canvas, Canvas traceCanvas) {
        if (nanoTime() - tStart > 2e9) {
            if (Double.compare(startTimeSim, 0) == 0) {
                startTimeSim = nanoTime();
                canvas.getGraphicsContext2D().clearRect(0, 0, scene.getWidth(), scene.getHeight());
                traceCanvas.getGraphicsContext2D().clearRect(0, 0, scene.getWidth(), scene.getHeight());
                LOGGER.info("Animation started...");
            }
            drawAnimation.drawPendulum(canvas, traceCanvas);
            statsController.getStats(runTimeSim, canvas);
            runTimeSim = (nanoTime() - startTimeSim) / 1e9;
        }
    }

    public void updateFrameIfPaused() {
        if (Objects.equals(getTimeline().getStatus(), PAUSED)) {
            manageSim(canvas, traceCanvas);
        }
    }

    public void restartSim() {
        double stopTimeSim = nanoTime();
        final var message = "Animation finished.%nAnimation Runtime: %.3f seconds%n%s%n"
                .formatted((stopTimeSim - startTimeSim) / 1e9, DOTTED_LINE);
        LOGGER.info(message);
        timeline.stop();
        reset(true);
        start();
        tStart = nanoTime();
    }

    public void reset(boolean isRestart) {
        startTimeSim = tStart = 0;
        drawAnimation.resetAnimationVars(isRestart);
        colorController.resetColors();
    }

    private void printClosingText() {
        double stopTimeSim = nanoTime();
        final var closingMessage = "%s%nAnimation runtime of instance %d: %.2f seconds%n%s%n"
                .formatted(CLOSING_MESSAGE, instance, (stopTimeSim - startTimeSim) / 1e9, DOTTED_LINE);
        LOGGER.info(closingMessage);
    }

    public UserInputController getUserInputController() {
        return userInputController;
    }

    public Scene getScene() {
        return scene;
    }

    public StackPane getRoot() {
        return root;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public Canvas getTraceCanvas() {
        return traceCanvas;
    }

    public Timeline getTimeline() {
        return timeline;
    }
}
