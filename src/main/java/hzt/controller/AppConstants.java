package hzt.controller;

import hzt.model.Coordinate;
import javafx.geometry.Point2D;

import java.awt.*;

public class AppConstants {

    // constants
    static final String TITLE = "Double Pendulum";
    static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    public static final int PREF_WIDTH_MENU = 250;
    static final int FONT_SIZE_ANNOUNCEMENTS = 80;
    static final int INIT_FRAME_RATE = 30;
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final Dimension INIT_SCENE_SIZE = new Dimension(SCREEN_SIZE.width * 7 / 12, SCREEN_SIZE.height * 9 / 12);
    public static final Point2D INIT_ORIGIN = new Coordinate(INIT_SCENE_SIZE.width / 2., INIT_SCENE_SIZE.height / 2.);


    // http://patorjk.com/software/taag/#p=display&f=Big&t=A%20%20'Image'%20ng

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BRIGHT_PURPLE = "\u001B[95m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_GREEN = "\u001B[32m";

    public static final String ANSI_BRIGHT_RED = "\u001B[91m";

    static final String TITLE_FX = ANSI_GREEN +
            "Double Pendulum" +
            ANSI_RESET;

    static final String CLOSING_MESSAGE = ANSI_BLUE +
            "See you next Time! :)" +
            ANSI_RESET;
}
