# Double Pendulum

---

## About
This is a simulation program of a double pendulum motion.

It uses differential equations to determine the motion of the two beams.

They are solved in real time by simple discrete Euler methods. 

So the precision is not very accurate.

![example1](src/main/resources/image/example.png)

# Setup
Before you run the program, make sure you modify the following in Intellij:
- Go to File -->  Settings --> Appearance and Behaviour --> Path Variables
- Add a new path variable by clicking on the + sign and name it PATH_TO_FX and browse to the lib folder of the JavaFX SDK to set its value, and click apply. 
- Go to Run --> Edit Configurations
- Type the following in the VM Options section: --module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml
- Click Apply and Run the application. It should work now.

[source](https://openjfx.io/openjfx-docs/#IDE-Intellij)

## Equations
![double pendulum](src/main/resources/image/double pendulum.png)

Enjoy!

![equations](src/main/resources/image/equations.png)

In physics and mathematics, in the area of dynamical systems. 
A double pendulum is a pendulum with another pendulum attached to its end, 
and is a simple physical system that exhibits rich dynamic behavior with a strong sensitivity to initial conditions.[1] 
The motion of a double pendulum is governed by a set of coupled ordinary differential equations and is chaotic.
Contents

    1 Analysis and interpretation
        1.1 Lagrangian
    2 Chaotic motion
    3 See also
    4 Notes
    5 References
    6 External links

Analysis and interpretation

Several variants of the double pendulum may be considered; the two limbs may be of equal or unequal lengths and masses, they may be simple pendulums or compound pendulums (also called complex pendulums) and the motion may be in three dimensions or restricted to the vertical plane. In the following analysis, the limbs are taken to be identical compound pendulums of length l and mass m, and the motion is restricted to two dimensions.
Double compound pendulum
Motion of the double compound pendulum (from numerical integration of the equations of motion)
Trajectories of a double pendulum

In a compound pendulum, the mass is distributed along its length. If the mass is evenly distributed, then the center of mass of each limb is at its midpoint, and the limb has a moment of inertia of I = 1/12ml2 about that point.

It is convenient to use the angles between each limb and the vertical as the generalized coordinates defining the configuration of the system. These angles are denoted θ1 and θ2. The position of the center of mass of each rod may be written in terms of these two coordinates. If the origin of the Cartesian coordinate system is taken to be at the point of suspension of the first pendulum, then the center of mass of this pendulum is at:

    x 1 = l 2 sin ⁡ θ 1 y 1 = − l 2 cos ⁡ θ 1 {\displaystyle {\begin{aligned}x_{1}&={\frac {l}{2}}\sin \theta _{1}\\y_{1}&=-{\frac {l}{2}}\cos \theta _{1}\end{aligned}}} {\displaystyle {\begin{aligned}x_{1}&={\frac {l}{2}}\sin \theta _{1}\\y_{1}&=-{\frac {l}{2}}\cos \theta _{1}\end{aligned}}}

and the center of mass of the second pendulum is at

    x 2 = l ( sin ⁡ θ 1 + 1 2 sin ⁡ θ 2 ) y 2 = − l ( cos ⁡ θ 1 + 1 2 cos ⁡ θ 2 ) {\displaystyle {\begin{aligned}x_{2}&=l\left(\sin \theta _{1}+{\tfrac {1}{2}}\sin \theta _{2}\right)\\y_{2}&=-l\left(\cos \theta _{1}+{\tfrac {1}{2}}\cos \theta _{2}\right)\end{aligned}}} {\displaystyle {\begin{aligned}x_{2}&=l\left(\sin \theta _{1}+{\tfrac {1}{2}}\sin \theta _{2}\right)\\y_{2}&=-l\left(\cos \theta _{1}+{\tfrac {1}{2}}\cos \theta _{2}\right)\end{aligned}}}

This is enough information to write out the Lagrangian.
Lagrangian

The Lagrangian is

    L = kinetic energy − potential energy = 1 2 m ( v 1 2 + v 2 2 ) + 1 2 I ( θ ˙ 1 2 + θ ˙ 2 2 ) − m g ( y 1 + y 2 ) = 1 2 m ( x ˙ 1 2 + y ˙ 1 2 + x ˙ 2 2 + y ˙ 2 2 ) + 1 2 I ( θ ˙ 1 2 + θ ˙ 2 2 ) − m g ( y 1 + y 2 ) {\displaystyle {\begin{aligned}L&={\text{kinetic energy}}-{\text{potential energy}}\\&={\tfrac {1}{2}}m\left(v_{1}^{2}+v_{2}^{2}\right)+{\tfrac {1}{2}}I\left({{\dot {\theta }}_{1}}^{2}+{{\dot {\theta }}_{2}}^{2}\right)-mg\left(y_{1}+y_{2}\right)\\&={\tfrac {1}{2}}m\left({{\dot {x}}_{1}}^{2}+{{\dot {y}}_{1}}^{2}+{{\dot {x}}_{2}}^{2}+{{\dot {y}}_{2}}^{2}\right)+{\tfrac {1}{2}}I\left({{\dot {\theta }}_{1}}^{2}+{{\dot {\theta }}_{2}}^{2}\right)-mg\left(y_{1}+y_{2}\right)\end{aligned}}} {\displaystyle {\begin{aligned}L&={\text{kinetic energy}}-{\text{potential energy}}\\&={\tfrac {1}{2}}m\left(v_{1}^{2}+v_{2}^{2}\right)+{\tfrac {1}{2}}I\left({{\dot {\theta }}_{1}}^{2}+{{\dot {\theta }}_{2}}^{2}\right)-mg\left(y_{1}+y_{2}\right)\\&={\tfrac {1}{2}}m\left({{\dot {x}}_{1}}^{2}+{{\dot {y}}_{1}}^{2}+{{\dot {x}}_{2}}^{2}+{{\dot {y}}_{2}}^{2}\right)+{\tfrac {1}{2}}I\left({{\dot {\theta }}_{1}}^{2}+{{\dot {\theta }}_{2}}^{2}\right)-mg\left(y_{1}+y_{2}\right)\end{aligned}}}

The first term is the linear kinetic energy of the center of mass of the bodies and the second term is the rotational kinetic energy around the center of mass of each rod. The last term is the potential energy of the bodies in a uniform gravitational field. The dot-notation indicates the time derivative of the variable in question.

Substituting the coordinates above and rearranging the equation gives

    L = 1 6 m l 2 ( θ ˙ 2 2 + 4 θ ˙ 1 2 + 3 θ ˙ 1 θ ˙ 2 cos ⁡ ( θ 1 − θ 2 ) ) + 1 2 m g l ( 3 cos ⁡ θ 1 + cos ⁡ θ 2 ) . {\displaystyle L={\tfrac {1}{6}}ml^{2}\left({{\dot {\theta }}_{2}}^{2}+4{{\dot {\theta }}_{1}}^{2}+3{{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\cos(\theta _{1}-\theta _{2})\right)+{\tfrac {1}{2}}mgl\left(3\cos \theta _{1}+\cos \theta _{2}\right).} {\displaystyle L={\tfrac {1}{6}}ml^{2}\left({{\dot {\theta }}_{2}}^{2}+4{{\dot {\theta }}_{1}}^{2}+3{{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\cos(\theta _{1}-\theta _{2})\right)+{\tfrac {1}{2}}mgl\left(3\cos \theta _{1}+\cos \theta _{2}\right).}

There is only one conserved quantity (the energy), and no conserved momenta. The two momenta may be written as

    p θ 1 = ∂ L ∂ θ ˙ 1 = 1 6 m l 2 ( 8 θ ˙ 1 + 3 θ ˙ 2 cos ⁡ ( θ 1 − θ 2 ) ) p θ 2 = ∂ L ∂ θ ˙ 2 = 1 6 m l 2 ( 2 θ ˙ 2 + 3 θ ˙ 1 cos ⁡ ( θ 1 − θ 2 ) ) . {\displaystyle {\begin{aligned}p_{\theta _{1}}&={\frac {\partial L}{\partial {{\dot {\theta }}_{1}}}}={\tfrac {1}{6}}ml^{2}\left(8{{\dot {\theta }}_{1}}+3{{\dot {\theta }}_{2}}\cos(\theta _{1}-\theta _{2})\right)\\p_{\theta _{2}}&={\frac {\partial L}{\partial {{\dot {\theta }}_{2}}}}={\tfrac {1}{6}}ml^{2}\left(2{{\dot {\theta }}_{2}}+3{{\dot {\theta }}_{1}}\cos(\theta _{1}-\theta _{2})\right).\end{aligned}}} {\displaystyle {\begin{aligned}p_{\theta _{1}}&={\frac {\partial L}{\partial {{\dot {\theta }}_{1}}}}={\tfrac {1}{6}}ml^{2}\left(8{{\dot {\theta }}_{1}}+3{{\dot {\theta }}_{2}}\cos(\theta _{1}-\theta _{2})\right)\\p_{\theta _{2}}&={\frac {\partial L}{\partial {{\dot {\theta }}_{2}}}}={\tfrac {1}{6}}ml^{2}\left(2{{\dot {\theta }}_{2}}+3{{\dot {\theta }}_{1}}\cos(\theta _{1}-\theta _{2})\right).\end{aligned}}}

These expressions may be inverted to get

    θ ˙ 1 = 6 m l 2 2 p θ 1 − 3 cos ⁡ ( θ 1 − θ 2 ) p θ 2 16 − 9 cos 2 ⁡ ( θ 1 − θ 2 ) θ ˙ 2 = 6 m l 2 8 p θ 2 − 3 cos ⁡ ( θ 1 − θ 2 ) p θ 1 16 − 9 cos 2 ⁡ ( θ 1 − θ 2 ) . {\displaystyle {\begin{aligned}{{\dot {\theta }}_{1}}&={\frac {6}{ml^{2}}}{\frac {2p_{\theta _{1}}-3\cos(\theta _{1}-\theta _{2})p_{\theta _{2}}}{16-9\cos ^{2}(\theta _{1}-\theta _{2})}}\\{{\dot {\theta }}_{2}}&={\frac {6}{ml^{2}}}{\frac {8p_{\theta _{2}}-3\cos(\theta _{1}-\theta _{2})p_{\theta _{1}}}{16-9\cos ^{2}(\theta _{1}-\theta _{2})}}.\end{aligned}}} {\displaystyle {\begin{aligned}{{\dot {\theta }}_{1}}&={\frac {6}{ml^{2}}}{\frac {2p_{\theta _{1}}-3\cos(\theta _{1}-\theta _{2})p_{\theta _{2}}}{16-9\cos ^{2}(\theta _{1}-\theta _{2})}}\\{{\dot {\theta }}_{2}}&={\frac {6}{ml^{2}}}{\frac {8p_{\theta _{2}}-3\cos(\theta _{1}-\theta _{2})p_{\theta _{1}}}{16-9\cos ^{2}(\theta _{1}-\theta _{2})}}.\end{aligned}}}

The remaining equations of motion are written as

    p ˙ θ 1 = ∂ L ∂ θ 1 = − 1 2 m l 2 ( θ ˙ 1 θ ˙ 2 sin ⁡ ( θ 1 − θ 2 ) + 3 g l sin ⁡ θ 1 ) p ˙ θ 2 = ∂ L ∂ θ 2 = − 1 2 m l 2 ( − θ ˙ 1 θ ˙ 2 sin ⁡ ( θ 1 − θ 2 ) + g l sin ⁡ θ 2 ) . {\displaystyle {\begin{aligned}{{\dot {p}}_{\theta _{1}}}&={\frac {\partial L}{\partial \theta _{1}}}=-{\tfrac {1}{2}}ml^{2}\left({{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\sin(\theta _{1}-\theta _{2})+3{\frac {g}{l}}\sin \theta _{1}\right)\\{{\dot {p}}_{\theta _{2}}}&={\frac {\partial L}{\partial \theta _{2}}}=-{\tfrac {1}{2}}ml^{2}\left(-{{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\sin(\theta _{1}-\theta _{2})+{\frac {g}{l}}\sin \theta _{2}\right).\end{aligned}}} {\displaystyle {\begin{aligned}{{\dot {p}}_{\theta _{1}}}&={\frac {\partial L}{\partial \theta _{1}}}=-{\tfrac {1}{2}}ml^{2}\left({{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\sin(\theta _{1}-\theta _{2})+3{\frac {g}{l}}\sin \theta _{1}\right)\\{{\dot {p}}_{\theta _{2}}}&={\frac {\partial L}{\partial \theta _{2}}}=-{\tfrac {1}{2}}ml^{2}\left(-{{\dot {\theta }}_{1}}{{\dot {\theta }}_{2}}\sin(\theta _{1}-\theta _{2})+{\frac {g}{l}}\sin \theta _{2}\right).\end{aligned}}}

These last four equations are explicit formulae for the time evolution of the system given its current state. It is not possible to go further and integrate these equations analytically, to get formulae for θ1 and θ2 as functions of time. It is, however, possible to perform this integration numerically using the Runge Kutta method or similar techniques.
Chaotic motion
Graph of the time for the pendulum to flip over as a function of initial conditions
Long exposure of double pendulum exhibiting chaotic motion (tracked with an LED)

The double pendulum undergoes chaotic motion, and shows a sensitive dependence on initial conditions. The image to the right shows the amount of elapsed time before the pendulum flips over, as a function of initial position when released at rest. Here, the initial value of θ1 ranges along the x-direction from −3 to 3. The initial value θ2 ranges along the y-direction, from −3 to 3. The colour of each pixel indicates whether either pendulum flips within:

    10√​l⁄g (green)
    100√​l⁄g (red)
    1000√​l⁄g (purple) or
    10000√​l⁄g (blue).

Three double pendulums with near identical initial conditions diverge over time displaying the chaotic nature of the system.

Initial conditions that do not lead to a flip within 10000√​l⁄g are plotted white.

The boundary of the central white region is defined in part by energy conservation with the following curve:

    3 cos ⁡ θ 1 + cos ⁡ θ 2 = 2. {\displaystyle 3\cos \theta _{1}+\cos \theta _{2}=2.} {\displaystyle 3\cos \theta _{1}+\cos \theta _{2}=2.}

Within the region defined by this curve, that is if

    3 cos ⁡ θ 1 + cos ⁡ θ 2 > 2 , {\displaystyle 3\cos \theta _{1}+\cos \theta _{2}>2,} {\displaystyle 3\cos \theta _{1}+\cos \theta _{2}>2,}

then it is energetically impossible for either pendulum to flip. Outside this region, the pendulum can flip, but it is a complex question to determine when it will flip. Similar behavior is observed for a double pendulum composed of two point masses rather than two rods with distributed mass.[2]

The lack of a natural excitation frequency has led to the use of double pendulum systems in seismic resistance designs in buildings, where the building itself is the primary inverted pendulum, and a secondary mass is connected to complete the double pendulum. 

---

## Notes

--- 

Enjoy!

